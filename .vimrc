set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'sickill/vim-monokai'
Plugin 'dylanaraps/wal.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'tpope/vim-commentary'
Plugin 'vim-airline/vim-airline'

Plugin 'tmhedberg/SimpylFold'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'vim-python/python-syntax'

Plugin 'scrooloose/nerdtree'
Plugin 'wakatime/vim-wakatime'
" Plugin 'craigemery/vim-autotag'
" Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'lervag/vimtex'
Plugin 'xuhdev/vim-latex-live-preview'
Plugin 'luochen1990/rainbow'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'

call vundle#end()            " required
filetype plugin indent on    " required
filetype on

" Leaders / Escape
let mapleader = " "
let maplocalleader = "\\"

" Display settings

set number       " Affiche les numéros de ligne
set relativenumber
set scrolloff=3  " Laisse 3 lines en bas/haut en scrollant
set noruler      " N'affiche pas la position du curseur
set nowrap
set showcmd	" Show typed cmd in status bar
set showmatch   " Montre la parenthese correspondante
syntax on
" colorscheme monokai
colorscheme wal
set conceallevel=2  " Markdown

let g:rainbow_active = 1  " Rainbow super swaggy parenthesis
let g:python_highlight_all = 1

" Indentation / Editor

set smartindent " Indentation intelligente
set autoindent  " Conserve l'indentation sur une nouvelle ligne
set encoding=utf-8
set ignorecase          " case insensitive searching
set smartcase           " but become case sensitive if you type uppercase characters
set tabstop=4
set softtabstop=4
set shiftwidth=4

fun! <SID>StripTrailingWhitespaces()
	let l = line(".")
	let c = col(".")
	%s/\s\+$//e
	call cursor(l, c)
endfun
autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()

" Enable folding

set foldmethod=indent
set foldlevel=99
let g:SimpylFold_docstring_preview = 1

" Completion and snipets
let g:UltiSnipsExpandTrigger = "<c-space>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsEditSplit = "vertical"
let g:UltiSnipsSnippetDirectories=["UltiSnips", "/home/diego/dotfiles/ultisnips/"]

nnoremap <leader>s :UltiSnipsEdit<cr>
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

" Manage my vimrc
nnoremap <leader>ve :vsplit $MYVIMRC<cr>
noremap <leader>vs :source ~/.vimrc<CR>
noremap <buffer> <leader>ZZ ZZ:source ~/.vimrc<CR>

" Big grid move

" nnoremap H ^
" nnoremap L $
" nnoremap J 15jzz
" nnoremap K 15kzz

" Disable arrows

noremap <left> <nop>
noremap <right> <nop>
noremap <up> <nop>
noremap <down> <nop>

" Split navigations

nnoremap J <C-W><C-J>
nnoremap K <C-W><C-K>
nnoremap L <C-W><C-L>
nnoremap H <C-W><C-H>

" NERDTree

map <c-t> :NERDTreeToggle<cr>
autocmd StdinReadPre * let s:std_in=1  " open nerdtree when vim open on folder
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Airline
let g:airline_powerline_fonts = 1

" Abreviations

iabbrev witdh width
iabbrev widht width
iabbrev lenght length

" Shortcuts

" Reindentation
nnoremap <leader>i mzgg=G`zzz

" Python

augroup python
	autocmd!
	autocmd FileType python nnoremap <buffer> <leader>c I# <esc>
	autocmd FileType python iabbrev <buffer> whiel while
	autocmd FileType python iabbrev <buffer> iff if:<left>
	autocmd FileType python noremap <buffer> <F5> <esc>:w!<CR>:!python3 %<CR>
	autocmd BufNewFile,BufRead python
				\ set tabstop=4
				\ set softtabstop=4
				\ set shiftwidth=4
				\ set expandtab
				\ set autoindent
				\ set fileformat=unix
	autocmd FileType python nnoremap <buffer> <c-<F5>> <esc>:w<cr>:!pytest<cr>
	autocmd FileType python iabbrev <buffer> #! #!/usr/bin/python3
	autocmd FileType python iabbrev <buffer> ifmain if __name__ == '__main__':<CR>    main()<esc>kki
augroup END

" LaTeX

let g:tex_flavor = 'latex'
let g:vimtex_compiler_method = 'latexmk'

augroup latex
	autocmd!
	autocmd FileType tex set wrap
	autocmd FileType tex set sw=2
	autocmd FileType tex nnoremap <F5> :w<cr>:!pdflatex "%"<cr>
	autocmd FileType tex nnoremap <leader>ll :LLPStartPreview<cr>
	autocmd FileType tex iabbrev ssi si et seulement si
	autocmd FileType tex iabbrev mtn maintenant
augroup END

" C++

let g:ycm_global_ycm_extra_conf = '~/dotfiles/vim/ycm_global_conf.py'
let g:ycm_confirm_extra_conf = 0

augroup cpp
	autocmd!
	autocmd FileType cpp nnoremap <buffer> <F4> :w<CR>:!g++ -Wall -std=c++11 "%:p" -o "%:p:r.out"<CR>
	autocmd FileType cpp nmap <buffer> <F5> <F4>:!"%:p:r.out"<CR>
	autocmd FileType cpp nnoremap <buffer> ;; A;<ESC>
	autocmd BufNewFile *.cpp 0r ~/dotfiles/vim/templates/skeleton.cpp
	" autocmd FileType cpp iabbrev <buffer> #inc #include <><left>
	autocmd FileType cpp iabbrev <buffer> elif else if () {<cr>}<esc>k$2hi
	autocmd FileType cpp iabbrev <buffer> if if () {<cr>}<esc>k3la
	autocmd FileType cpp iabbrev <buffer> { {<CR>}<ESC>kA
augroup END

