ln -s ~/dotfiles/.vimrc ~/.vimrc
ln -s ~/dotfiles/.bashrc ~/.bashrc
ln -s ~/dotfiles/.bash_aliases ~/.bash_aliases

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# for mypowerline
sudo apt install python3-pip
pip3 install ansimarkup click

# font
cp "./Monaco Nerd Font Complete Mono.ttf" ~/.fonts

