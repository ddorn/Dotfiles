alias o=xdg-open
alias py=python3
alias bud=~/prog/budget/budget.py
alias hue=~/prog/hue/hue.py
alias shortcuts='sudo ~/prog/hue/shortcuts.py && (sudo ~/prog/hue/shortcuts.py &)'

alias rot="xrandr -o left"
alias norot="xrandr -o normal"

alias jobs='jobs -lr'
alias cleantex="rm *.out *.aux *.gz *.log"
alias zulip="~/.appImage/Zulip-2.3.82-x86_64.AppImage &"
alias aka="vim ~/.bash_aliases && source ~/.bash_aliases"
