#/usr/bin/python3

"""
Script to generate a colorful prompt with informations.

Requires Python 3.6
"""

import os
import subprocess
import time
import shutil
import pwd

from ansimarkup import AnsiMarkup
import click

BASHPID = 0

FULL_SEP = ''
FULL_SEP_LEFT = ''
SOFT_SEP = ''
SOFT_SEP_LEFT = ''

if False:
    FULL_SEP = ' '
    FULL_SEP_LEFT = ' '
    SOFT_SEP = ' '
    SOFT_SEP_LEFT = ' '

BATTERY_STATUS = ''

MARKUP = AnsiMarkup({"fg None": "\033[39m",
                     "bg None": "\033[49m"})

def soft_sep(color, left=False):
    sep = SOFT_SEP_LEFT if left else SOFT_SEP
    return f' <fg {color}>{sep} '


def full_sep(left_color, right_color, left=False):
    if left:
        return f' <bg {left_color}><fg {right_color}>{FULL_SEP_LEFT}<bg {right_color}> '
    return f' <fg {left_color}><bg {right_color}>{FULL_SEP} '


def two_digits(x, n=2):
    x = str(x)
    return '0' * (n - len(x)) + x


class Segment:
    def __init__(self, fg, bg):
        self.fg = fg
        self.bg = bg
        self.visible = True

    def __str__(self):
        return f'<fg {self.fg}><bg {self.bg}>{self.text}'

    @property
    def text(self):
        return NotImplemented


class Prompt:
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __str__(self):
        self.update_visibility()

        left_segments = [s for s in self.left if s.visible]
        right_segments = [s for s in self.right if s.visible]

        # Joining left segments

        if left_segments:
            left = f'<bg {left_segments[0].bg}> '
            for i, s in enumerate(left_segments):
                next_bg = None if i == len(left_segments) - 1 else left_segments[i + 1].bg
                left += str(s)
                left += full_sep(s.bg, next_bg)
        else:
            left = ''

        # We do the right from right to left

        if right_segments:
            right = ' '
            for i, s in enumerate(right_segments[::-1]):
                next_bg = None if i == len(right_segments) - 1 else right_segments[-i - 2].bg
                right = str(s) + right
                right = full_sep(next_bg, s.bg, True) + right
        else:
            right = ''

        # Computing number of spaces between left and right

        width = int(subprocess.check_output(['stty', 'size']).decode().partition(' ')[2])
        prompt = left + ' ' * (width - len(MARKUP.strip(left + right))) + right

        # we reset the colors at the end of the prompt

        prompt += '<fg None><bg None>'
        return prompt

    def update_visibility(self):
        pass

class PwdSegment(Segment):

    def __init__(self, fg=3, bg=0, sep='#808080'):
        super().__init__(fg, bg)
        self.sep = sep

    @property
    def text(self):
        segments = os.path.abspath('.').replace(os.path.expanduser('~'), '~').split(os.path.sep)

        segments_kept = []
        path_size = 0
        for i, s in enumerate(segments[::-1]):
            path_size += len(s)
            if path_size > 20 and segments_kept:  # we want to have at least the folder name even if it's big
                segments_kept.insert(0, f'<fg {self.fg}><bg {self.bg}>…')
                break
            segments_kept.insert(0, f'<fg {self.fg}><bg {self.bg}>'+ s)

        return soft_sep(self.sep).join(segments_kept)


class UserNameSegment(Segment):
    def __init__(self, fg=7, bg=6):
        super().__init__(fg, bg)

    @property
    def text(self):
        return pwd.getpwuid(os.getuid())[0].capitalize()


class GitSegment(Segment):
    def __init__(self, fg=2, bg='#505050', fg_dirty=7, bg_dirty=1):
        self.status = self.get_git_status().splitlines()

        if not self.status:
            self.visible = False
        else:
            self.dirty = len(self.status) > 1
            if self.dirty:
                super().__init__(fg_dirty, bg_dirty)
            else:
                super().__init__(fg, bg)

    def get_git_status(self):
        return subprocess.run('git status -bs'.split(), stdout=subprocess.PIPE, stderr=subprocess.DEVNULL).stdout.decode()

    @property
    def text(self):
        branch = self.status[0].partition('...')[0][3:]

        ncommits_to_push = self.status[0].partition('ahead ')[2][:-1]
        if ncommits_to_push:
            to_push = f' {ncommits_to_push}↑'
        else:
            to_push = ''
        return f'({branch})' + to_push


class ExcecTimeSegment(Segment):
    def __init__(self, fg=7, bg='#013367'):
        super().__init__(fg, bg)

    @property
    def text(self):
        t = load()

        return "{}:{}.{}".format(two_digits(int(t // 60 % 60)),
                                 two_digits(int(t % 60)),
                                 two_digits(int(t * 1000 % 1000), 4))

class BatterySegment(Segment):
    def __init__(self, fg=7, bg='#2ECC71', fg_crit=1, bg_crit='#FFAF00', crit=25, charged=90):
        self.crit = crit
        self.charged = charged

        with open(r'/sys/class/power_supply/BAT0/capacity') as f:
            self.battery = int(f.read())
        with open('/sys/class/power_supply/BAT0/status') as f:
            self.charging = f.read() == 'Charging\n'

        if self.battery <= crit:
            fg = fg_crit
            bg = bg_crit
        elif bg[0] == bg_crit[0] == '#':
            bg = [int(bg[x:x+2], 16) for x in range(1, 6, 2)]
            bg_crit = [int(bg_crit[x:x+2], 16) for x in range(1, 6, 2)]

            coeff = (self.battery - crit) / (self.charged - crit)
            coeff = 1 if coeff > 1 else coeff

            for i in range(3):
                a, b = bg[i], bg_crit[i]
                bg[i] = int(a * coeff + b * (1 - coeff))
            bg = '#' + ''.join(two_digits(hex(x)[2:]) for x in bg)

        super().__init__(fg, bg)

    @property
    def text(self):
        if self.battery < self.crit:
            text = BATTERY_STATUS[0]
        elif self.battery > self.charged:
            text = BATTERY_STATUS[-2]
        elif self.charging:
            text = BATTERY_STATUS[-1]
        else:
            text = BATTERY_STATUS[1 + self.battery // len(BATTERY_STATUS)]

        return text


class MyPrompt(Prompt):
    def __init__(self):
        left = [
            UserNameSegment(),
            PwdSegment(),
            GitSegment()
        ]
        right = [
            BatterySegment(),
            ExcecTimeSegment()
        ]

        super().__init__(left, right)


def load():
    TMP_FOLDER = os.path.expanduser('~/dotfiles/tmp/')

    if not os.path.exists(TMP_FOLDER):
        os.mkdir(TMP_FOLDER)

    if os.path.exists(TMP_FOLDER + str(BASHPID)):
        with open(TMP_FOLDER + str(BASHPID), 'r') as f:
            timestamp = f.read()
    else:
        timestamp = '!0'

    if timestamp.startswith('!'):
        timestamp = float(timestamp[1:])
    else:
        timestamp = time.time() - float(timestamp)

    save('!' + str(timestamp))
    return timestamp


def save(timestamp):
    TMP_FOLDER = os.path.expanduser('~/dotfiles/tmp/')

    if not os.path.exists(TMP_FOLDER):
        os.mkdir(TMP_FOLDER)

    with open(TMP_FOLDER + str(BASHPID), 'w') as f:
        f.write('{}\n'.format(timestamp))


def update_for_prompt():

    now = time.time()
    save(now)


@click.command()
@click.argument('bashpid', default=0)
@click.option('--update', is_flag=True, help='Update parameters for a shell, doesn\'t print the prompt')
def main(bashpid, update):
    global BASHPID

    BASHPID = bashpid

    if update:
        update_for_prompt()
    else:
        MARKUP.ansiprint(str(MyPrompt()))

if __name__ == '__main__':
    main()

